package com.formation.espi.java.superherogestion.model;

public enum SuperHerosType {
    VOLANT(true),
    SANS_POUVOIR(false);

    private boolean canFly;

    SuperHerosType(boolean canFly) {
        this.canFly = canFly;
    }

    boolean getCanFly() {
        return this.canFly;
    }

}