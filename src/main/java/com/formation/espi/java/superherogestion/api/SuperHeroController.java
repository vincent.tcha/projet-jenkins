package com.formation.espi.java.superherogestion.api;


import com.formation.espi.java.superherogestion.api.dto.SuperHeroDTO;
import com.formation.espi.java.superherogestion.model.SuperHero;
import com.formation.espi.java.superherogestion.repository.SuperHeroRepository;
import lombok.RequiredArgsConstructor;
import net.bytebuddy.implementation.bind.annotation.Super;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.badRequest;

@RestController
@RequestMapping(
        path = "/superHeroes",
        produces = {MediaType.APPLICATION_JSON_VALUE}
)


public class SuperHeroController<name_heros, super_heros_name> {

    private final SuperHeroRepository superHeroRepository;

    SuperHeroController(
            SuperHeroRepository superHeroRepository
    ) {
        this.superHeroRepository = superHeroRepository;
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<SuperHeroDTO> getById(@PathVariable Long id) {
//        Optional<SuperHero> optionalSuperHero = this.superHeroRepository.findById(id);
//        if (optionalSuperHero.isPresent()) {
//            SuperHero superHero = optionalSuperHero.get();
//            SuperHeroDTO superHeroDTO = mapToDTO(superHero);
//            return ResponseEntity.ok(superHeroDTO);
//        } else {
//            return ResponseEntity.notFound().build();
//        }

        return this.superHeroRepository.findById(id)
                .map(superHero -> ResponseEntity.ok(mapToDTO(superHero)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping
    public ResponseEntity<List<SuperHeroDTO>> getAll() {
//         List<SuperHero> superHeroes = this.superHeroRepository.findAll();
//         List<SuperHeroDTO> superHeroDTOS = new ArrayList<>();
//         superHeroes.forEach(superHero -> superHeroDTOS.add(mapToDTO(superHero)));
//
//        return ResponseEntity.ok(superHeroDTOS);
        return ResponseEntity.ok(
                this.superHeroRepository
                        .findAll()
                        .stream()
                        .map(this::mapToDTO)
                        .collect(Collectors.toList())
        );
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<SuperHeroDTO> create(@RequestBody SuperHeroDTO superHeroDTO) {
        superHeroDTO.setId(0);
        SuperHero superHero = mapToEntity(superHeroDTO);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(mapToDTO(this.superHeroRepository.save(superHero)));
    }


    @PutMapping(value = "/{id}")
    public ResponseEntity<SuperHeroDTO> update(
            @PathVariable Long id,
            @RequestBody SuperHeroDTO superHeroDTO) {
        if (this.superHeroRepository.findById(id).isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        if (id != superHeroDTO.getId()) {
            return ResponseEntity.badRequest().build();

        }
        SuperHero superHeroToUpdate = mapToEntity(superHeroDTO);
        return ResponseEntity.ok(mapToDTO(this.superHeroRepository.save(superHeroToUpdate)));

    }

    private SuperHero mapToEntity(SuperHeroDTO superHeroDTO) {
        SuperHero superHero = new SuperHero();
        superHero.setId(superHeroDTO.getId());
        superHero.setSuperHeroName(superHeroDTO.getHeroName());
        superHero.setSecretIdentity(superHeroDTO.getSecretIdentity());
        return superHero;
    }

    private SuperHeroDTO mapToDTO(SuperHero superHero) {
        return new SuperHeroDTO(
                superHero.getId(),
                superHero.getSuperHeroName(),
                superHero.getSecretIdentity()
        );
    }
}
