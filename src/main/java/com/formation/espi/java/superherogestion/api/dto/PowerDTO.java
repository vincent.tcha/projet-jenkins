package com.formation.espi.java.superherogestion.api.dto;

import jdk.jshell.Snippet;
import lombok.*;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class PowerDTO {
    private long id;
    private String name;
    private String description;


}

