package com.formation.espi.java.superherogestion.repository;

import com.formation.espi.java.superherogestion.model.SuperHero;
import com.formation.espi.java.superherogestion.api.dto.SuperHeroDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

public interface SuperHeroRepository extends JpaRepository<SuperHero, Long> {




}
