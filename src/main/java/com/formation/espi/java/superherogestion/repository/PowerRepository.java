package com.formation.espi.java.superherogestion.repository;

import com.formation.espi.java.superherogestion.model.Power;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PowerRepository extends JpaRepository<Power, Long> {

    List<Power> findAllByNameIsStartingWith(String name);
}
